
<?php require_once "./code.php"; ?>
<!-- General rule when we are closing the ?php tag, this is like the script tag, enter and escape writing html and php -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S1</title>
</head>
<body>
	<h1>Hello World!</h1>
	<h1>Echoing Values</h1>

	<p><?php echo 'Good day $name'; ?></p>
	<p><?php echo "Good day $name"; ?></p>
	<p><?php echo 'Hello, ' . $name . '.'; ?></p>
	<p><?= 'Hi, ' . $name . '.' ?></p>
	<!-- the above syntax is shorthand for php echo -->
	<p><?= HERO; ?></p>
	<p><?= $HERO; ?></p>
	<p><?= $isGraduating; ?></p>
	<p><?= $spouse; ?></p>
	<p><?= "This is not visible: boolean - $isFailed and null - $spouse"; ?></p>

	<p><?= var_dump($isFailed)?></p>
	<p><?= gettype($spouse)?></p>
	<p><?= var_dump($name)?></p>
	<p><?= var_dump($grades)?></p>

	<p><?= print_r($pokemon)?></p>

	<p><?= $pokemon[1]?></p>
	<p><?= $grades[3]?></p>

	<p><?= $gradesObj->firstGrading; ?></p>
	<p><?= $personObj->address->city; ?></p>

	<!-- to print object properties -->

	<p><?= var_export($personObj); ?></p>

	<h1>Operators</h1>
	<p>X: <?php echo $x; ?></p>
	<p>X: <?php echo $y; ?></p>

	<p>Is Legal Age: <?= var_dump($isLegalAge);?></p>
	<p>Is Legal Age: <?= var_dump($isRegistered);?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?= $x + $y;?></p>
	<p>Difference: <?= $x - $y;?></p>
	<p>Product: <?= $x * $y;?></p>
	<p>Quotient: <?= $x / $y;?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality: <?= var_dump($x == '1342.14');?></p>
	<p>Strict Equality: <?= var_dump($x === '1342.14');?></p>
	<p>Loose Inequality: <?= var_dump($x != '1342.14');?></p>
	<p>Strict Inequality: <?= var_dump($x !== '1342.14');?></p>

	<h3>Greater/Lesser Than Operators</h3>

	<p>Is Lesser: <?= var_dump($x < $y);?></p>
	<p>Is Greater: <?= var_dump($x > $y);?></p>
	<p>Is Lesser or Equal: <?= var_dump($x <= $y);?></p>
	<p>Is Greater or Equal: <?= var_dump($x >= $y);?></p>

	<h2>Logical Operators</h2>
	<p>Are All Requirements Met: <?= var_dump($isLegalAge && $isRegistered);?></p>
	<p>Are Some Requirements Met: <?= var_dump($isLegalAge || $isRegistered);?></p>
	<p>Are Some Requirements Not Met: <?= var_dump(!$isLegalAge && !$isRegistered);?></p>

	<h1>Function</h1>
	<p>Full Name: <?= getFullName('John','D.','Smith');?></p>

	<h1>Selection Control Structures</h1>
	<h2>If-Elseif-Else</h2>
	<p><?= determineTyphoonIntensity(300);?></p>

	<h2>Ternary (Is Underage?)</h2>
	<p>78: <?= var_dump(isUnderage(78));?></p>
	<p>17: <?= var_dump(isUnderage(17));?></p>

	<h2>Switch</h2>
	<p><?= determineComputerUser(2);?></p>

	<h2>Try-Catch-Finally</h2>

		<p><?= greeting(300); ?></p>
		<p><?= greeting("Howdy!!!"); ?></p>




</body>
</html>