<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Activity s01</title>
</head>
<body>
    <h1>Full Address</h1>
    <p><?= getFullAddress("Philippines", "Quezon City", "Metro Manila", "36F EcoBldg., Misamis St."); ?></p> 
    <p><?= getFullAddress("USA", "Salt Lake City", "Utah", "1036 U Bldg. 32nd Ave."); ?></p>

    <h1>Letter-Based Grading</h1>
    <p><?= getLetterGrade(87); ?></p>
    <p><?= getLetterGrade(94); ?></p>
    <p><?= getLetterGrade(74); ?></p>
</body>
</html>